class UsersController < ApplicationController
  before_filter :signed_in_user, only: [:edit, :update]
  before_filter :correct_user, only: [:edit, :update]

  def show
    @user = User.find(params[:id])
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      flash.now[:success] = "Welcome there !"
      redirect_to @user
    else
      render 'new'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def index
    @users = User.all
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      flash[:success] = "Profile updated."
      redirect_to @user
    else
      render 'edit'
    end
  end  

  private
    def user_params
      params.require(:user).permit(:pseudo, :email, :password, :password_confirmation)
    end
    
    def signed_in_user
      deny_access("Please sign in !", :signin) unless signed_in?
    end
    
    def correct_user
      @user = User.find(params[:id])
      deny_access("Cannot edit other users profile !", root_path) unless current_user?(@user)
    end
end