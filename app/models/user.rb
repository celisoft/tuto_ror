class User < ActiveRecord::Base
  attr_accessible :email, :pseudo, :password, :password_confirmation
  
  #require init of remember_token
  before_create :create_remember_token
  
  #store pseudos & emails in downcase
  before_save { self.pseudo = pseudo.downcase }
  before_save { self.email = email.downcase }
  
  validates :pseudo, presence: true, uniqueness: { case_sensitive: false }, length: { maximum: 20 }
  validates :email, presence: true, uniqueness: { case_sensitive: false }, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }
  
  validates :password, presence: true, length: { minimum: 5 }
  validates :password_confirmation, presence: true, length: { minimum: 5 }
  
  has_secure_password
  
  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end
  
  def User.digest(token)
    Digest::SHA1.hexdigest(token.to_s)
  end  
  
  private
  
  def create_remember_token
    self.remember_token = User.digest(User.new_remember_token)
  end
end
