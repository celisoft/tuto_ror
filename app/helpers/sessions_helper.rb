module SessionsHelper
    def sign_in(user)
       remember_token = User.new_remember_token
       cookies.permanent[:remember_token] = remember_token
       user.update_attribute(:remember_token, User.digest(remember_token))
       self.current_user = user
    end
    
    def sign_out
        cookies.delete(:remember_token)
        self.current_user = nil
    end
    
    def signed_in?
        !current_user.nil?
    end
    
    def current_user=(user)
        @current_user = user
    end
    
    def current_user?(user)
        @current_user == user
    end
    
    def current_user
       remember_token = User.digest(cookies[:remember_token])
       @current_user ||= User.find_by_remember_token(remember_token)
    end
    
    def deny_access(msg, relocate_to)
        flash[:warning] = msg
        redirect_to relocate_to
    end
end
