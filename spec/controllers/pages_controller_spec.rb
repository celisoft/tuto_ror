require 'rails_helper'

RSpec.describe PagesController, :type => :controller do
  
  #Comment out if render required
  render_views
  
  describe "GET home" do
    it "returns http success (200)" do
      get :home
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET contact" do
    it "returns http success (200)" do
      get :contact
      expect(response).to have_http_status(:success)
    end
  end
  
  describe "GET about" do
    it "returns http success (200)" do
      get :about
      expect(response).to have_http_status(:success)
    end
  end

end
