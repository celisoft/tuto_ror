require 'spec_helper'

describe User do
  before { @myUser = User.new(pseudo: "Example User", email: "user@example.com", password: "totoro", password_confirmation: "totoro") }
  
  subject { @myUser }
  
  it {should respond_to(:pseudo) }
  it {should respond_to(:email) }
  it {should respond_to(:password_digest) }
  it {should respond_to(:password) }
  it {should respond_to(:password_confirmation) }
  it {should respond_to(:remember_token) }
  it {should respond_to(:authenticate) }
  
  it {should be_valid}
  
  describe "if pseudo is not defined" do
    before { @myUser.pseudo = " " }
    it {should_not be_valid}
  end
  
  describe "if email is not defined" do
    before { @myUser.email = " " }
    it {should_not be_valid}
  end

  describe "if pseudo is longer than 20 chars" do
    before { @myUser.pseudo = "a"*21 }
    it {should_not be_valid}
  end
  
  describe "if email is invalid (no @)" do
    before { @myUser.email = "bademail.fr" }
    it {should_not be_valid}
  end
  
  describe "if email is invalid (no user before @)" do
    before { @myUser.email = "@bademail.fr" }
    it {should_not be_valid}
  end
  
  describe "if email is invalid (bad domain)" do
    before { @myUser.email = "toto@bademail" }
    it {should_not be_valid}
  end
  
  describe "if email is valid (contains .)" do
    before { @myUser.email = "celine.liberal@celisoft.org" }
    it {should be_valid}
  end
  
  describe "if email is valid (contains _)" do
    before { @myUser.email = "celine_liberal@celisoft.org" }
    it {should be_valid}
  end
  
  describe "if password is not present" do
    before { @myUser = User.new(pseudo: "Toto", email: "toto@example.com", password: " ", password_confirmation: " ") }
    it { should_not be_valid }
  end
  
  describe "if password doesn't match confirmation" do
    before { @myUser.password_confirmation = "mismatch" }
    it { should_not be_valid }
  end
  
  describe "if password is shorter than 5 chars" do
    before { @myUser.password = @myUser.password_confirmation = "a"*4 }
    it {should_not be_valid}
  end
  
  describe "return value of authenticate method" do
    before { @myUser.save }
    let(:found_user) { User.find_by_email @myUser.email }
  
    describe "with valid password" do
      it { should eq found_user.authenticate(@myUser.password) }
    end
  
    describe "with invalid password" do
      it { should_not eq found_user.authenticate("invalid") }
    end
  end
  
  describe "remember token" do
    before { @myUser.save }
    its(:remember_token) { should_not be_blank }
  end
end